# Projeto Calculadora de Tinta

Este repositório contém o código-fonte de uma aplicação web desenvolvida em React que auxilia os usuários a calcular a quantidade de tinta necessária para pintar uma sala. A aplicação considera as dimensões das paredes, bem como a presença de janelas e portas, para fornecer uma estimativa precisa da quantidade de tinta necessária. Além disso, sugere o tamanho ideal das latas de tinta que devem ser compradas, priorizando sempre as latas maiores.

## Funcionalidades

- Cálculo da quantidade de tinta necessária com base nas medidas das paredes.
- Consideração de janelas e portas na área total a ser pintada.
- Sugestão de tamanhos de latas de tinta a serem compradas, priorizando as latas maiores.
- Interface amigável e fácil de usar.

## Estrutura do Projeto

A estrutura de pastas do projeto está organizada da seguinte forma:


## Bibliotecas Utilizadas

- `react-icons`: Para ícones na interface do usuário.
- `styled-components`: Para estilização dos componentes.
- `react-hot-toast`: Para notificações na aplicação.

## Instalação e Execução

Para rodar a aplicação localmente o FrontEnd, siga os seguintes passos:

1. Clone o repositório:
    ```sh
    git clone https://gitlab.com/gabrielmina118/case-digital-republic
    ```
2. Navegue até o diretório do projeto:
    ```sh
    cd paint-calculator
    ```
3. Instale as dependências:
    ```sh
    npm install
    ```
4. Inicie o servidor de desenvolvimento:
    ```sh
    npm start
    ```

A aplicação estará disponível em `http://localhost:3000`.


# Para rodar o Backend , siga o passo a passo:

1. Crie um arquivo .env com as seguintes variáveis:
    ```sh
    DB_HOST=db
    DB_USER=root
    DB_PASSWORD=password
    DB_NAME=mydatabase
    DB_PORTDB='3307'
    JWT_SECRET=your_jwt_secret
    ```
2. Execute o script:
    ```sh
    npm run dev ou execute o docker-compose up
    ```

## Uso da Aplicação

1. Insira as medidas de cada uma das quatro paredes.
2. Adicione o número de janelas e portas presentes em cada parede.
3. A aplicação calculará automaticamente a quantidade total de tinta necessária.
4. Será sugerida a combinação ideal de latas de tinta para compra, priorizando as latas maiores.

## Contribuição

Contribuições são bem-vindas! Sinta-se à vontade para abrir issues e enviar pull requests. Para maiores detalhes, consulte o arquivo [CONTRIBUTING.md](CONTRIBUTING.md).

## Licença

Este projeto está licenciado sob a Licença MIT. Consulte o arquivo [LICENSE](LICENSE) para obter mais informações.

---

Para dúvidas ou mais informações, entre em contato através do email `gabrielmina118@gmail.com`.

Agradecemos por utilizar nossa Calculadora de Tinta!
