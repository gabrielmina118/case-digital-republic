const express = require("express");
const bodyParser = require("body-parser");
const sequelize = require("./database");
const authRoutes = require("./routes/authRoutes");
require("dotenv").config();

const app = express();

app.use(bodyParser.json());

app.use("/auth", authRoutes);

const PORT = process.env.PORT || 3003;

sequelize
    .sync()
    .then(() => {
        app.listen(PORT, () => {
            console.log(`Server is running on port ${PORT}`);
        });
    })
    .catch((err) => {
        console.log("Error connecting to the database:", err);
    });
