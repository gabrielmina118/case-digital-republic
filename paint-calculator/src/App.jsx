import React, { useCallback, useState } from "react";
import WallForm from "./components/wallForm/WallForm";
import { validateWall } from "./utils/utils";
import Heading from "./components/heading/Heading";
import { Container, MainWall, WallContainer } from "./Styled";
import toast from "react-hot-toast";
import WallCard from "../src/components/wallCard/WallCard";
function App() {
    const [walls, setWalls] = useState([]);

    const addWall = useCallback((wall) => {
        try {            
            const area = validateWall(wall);
            setWalls((prevWalls) => [...prevWalls, { ...wall,area }]);
            toast.success("Adicionado com sucesso");
        } catch (err) {
            console.log(err.message);
            toast.error(err.message);
        }
    }, []);
    console.log("walls", walls);
    return (
        <Container>
            <Heading
                title={"Calculadora de tinta"}
                subtitle={"Calcule o total necessário de tinta"}
            />
            <WallForm addWall={addWall} />
            <WallContainer>
                {walls.map((wall) => {
                    return (
                        <MainWall>
                            <WallCard
                                id={wall.id}
                                area={wall.area}
                                height={wall.height}
                                width={wall.width}
                                windows={wall.windows}
                                doors={wall.doors}
                                walls={walls}
                            />
                        </MainWall>
                    );
                })}
            </WallContainer>
        </Container>
    );
}

export default App;
