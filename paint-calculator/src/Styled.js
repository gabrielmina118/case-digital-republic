import styled from "styled-components";

export const Container = styled.div`
    max-width: 2520px;
    margin: 0 auto;
    padding-left: 1rem; 
    padding-right: 1rem; 

    @media (min-width: 640px) {
        // sm breakpoint
        padding-left: 0.5rem; 
        padding-right: 0.5rem; 
    }

    @media (min-width: 768px) {
        // md breakpoint
        padding-left: 2.5rem; 
        padding-right: 2.5rem; 
    }

    @media (min-width: 1280px) {
        // xl breakpoint
        padding-left: 5rem; 
        padding-right: 5rem; 
    }
`;

export const WallContainer = styled.div`
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    width: 100%;
    justify-content: space-evenly;
    
    @media (max-width: 1200px) {
        grid-template-columns: repeat(3, 1fr);
    }

    @media (max-width: 900px) {
        grid-template-columns: repeat(2, 1fr);
    }

    @media (max-width: 600px) {
        grid-template-columns: 1fr;
    }
`;
export const MainWall = styled.div`
    display: flex;
    flex-direction: column;
    width: 70%;
`;

export const OpenButton = styled.button`
    margin: 20px;
    padding: 10px 20px;
    font-size: 16px;
    cursor: pointer;
`;