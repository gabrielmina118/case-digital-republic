import { Button } from "./Styled";

const ButtonCustom = ({ label, onClick, outline, small }) => {
    return (
        <Button onClick={onClick} outline={outline}>
            {label}
        </Button>
    );
};

export default ButtonCustom;
