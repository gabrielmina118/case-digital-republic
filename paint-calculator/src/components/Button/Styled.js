import styled from "styled-components";

export const Button = styled.button`
    position: relative;
    width: ${(props) => (props.outline ? "40%" : "80%")};
    border-radius: 0.5rem;
    transition: opacity 0.3s;
    background-color: ${(props) => (props.outline ? "white" : "#f43f5e")};
    border: ${(props) =>
        props.outline ? "1px solid black" : "2px solid #f43f5e"};
    color: ${(props) => (props.outline ? "black" : "white")};
    padding: ${(props) => (props.outline ? "0.25rem" : "0.75rem")};
    font-size: ${(props) => (props.outline ? "0.975rem" : "1rem")};
    font-weight: ${(props) => (props.outline ? "300" : "600")};
    height: 40px;
    cursor: pointer;

    &:disabled {
        opacity: 0.7;
        cursor: not-allowed;
    }
    &:hover {
        opacity: 0.8;
    }
`;
