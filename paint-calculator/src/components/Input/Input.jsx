import { useState } from 'react';
import { Container, Input, Label } from './Styled';

const InputCustom = ({ id, label, type , value , onChange }) => {
   const [isFocused, setIsFocused] = useState(false);

   const handleFocus = () => {
       setIsFocused(true);
   };

   const handleBlur = () => {
       setIsFocused(false);
   };

  return (
      <Container>
          <Label htmlFor={id} isFocused={isFocused}>
              {label}
          </Label>
          <Input
              id={id}
              type={type}
              onFocus={handleFocus}
              onBlur={handleBlur}
              value={value}
              onChange={onChange}
              required
          />
      </Container>
  );
}

export default InputCustom;
