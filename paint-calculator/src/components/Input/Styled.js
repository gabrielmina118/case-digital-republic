import styled from 'styled-components';

export const Container = styled.div`
  width: 60%;
  position: relative;
`;

export const Input = styled.input`
    width: 100%;
    padding: 0.3rem;
    border: 2px solid #999;
    border-radius: 0.25rem;
    transition: border-color 0.3s ease;
    font-size: 1.5rem;
    margin-bottom: 10px;
    &:focus {
        border-color: #333;
    }
`;
export const Label = styled.label`
    position: absolute;
    font-size: 1.5rem;
    top: 0.2rem;
    left: 0.2rem;
    transition: all 0.3s ease;
    color: ${(props) => (props.isFocused ? "#333" : "#999")};
    font-size: ${(props) => (props.isFocused ? "0.75rem" : "1rem")};
`;
