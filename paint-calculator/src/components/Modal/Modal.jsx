// Modal.js
import React from "react";
import {
    CloseButton,
    ModalBackground,
    ModalContent,
    PaintCanItem,
} from "./Styled";
import { FaPaintBrush } from "react-icons/fa";

const Modal = ({ show, handleClose, totalPaint, paintCans }) => {
    return (
        <ModalBackground show={show}>
            <ModalContent>
                <CloseButton onClick={handleClose}>&times;</CloseButton>
                <div>
                    <h2>
                        Total de tinta necessária:{" "}
                        {totalPaint && totalPaint.toFixed(2)} litros
                    </h2>
                    <h3>Latas de tinta sugeridas:</h3>
                    <ul>
                        {paintCans.map((can, index) => (
                            <PaintCanItem key={index}>
                                <FaPaintBrush />
                                {can} L
                            </PaintCanItem>
                        ))}
                    </ul>
                </div>
            </ModalContent>
        </ModalBackground>
    );
};

export default Modal;
