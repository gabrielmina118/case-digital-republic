import { HeadingContainer, Subtitle, Title } from "./Styled";

const Heading = ({ title, subtitle }) => {
    return (
        <HeadingContainer>
            <Title>{title}</Title>
            {subtitle && <Subtitle>{subtitle}</Subtitle>}
        </HeadingContainer>
    );
};

export default Heading;
