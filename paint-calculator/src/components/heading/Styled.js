import styled from "styled-components";

export const HeadingContainer = styled.div`
    text-align: center;
    font-size: larger;
`;

export const Title = styled.div`
    font-size: 2xl;
    font-weight: bold;
`;

export const Subtitle = styled.div`
    font-weight: light;
    color: #737373;
    margin-top: 0.5rem;
`;
