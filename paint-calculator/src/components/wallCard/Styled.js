import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 5px;
    background-color: #f0f0f0;
    border-radius: 8px;
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
    max-width: 200px;
    margin-bottom: 10px;

    @media (max-width: 1200px) {
        width: 100%;
        max-width: 500px;
    }

    @media (max-width: 900px) {
        width: 100%;
        max-width: 500px;
    }

    @media (max-width: 600px) {
        width: 100%;
        max-width: 500px;
    }
`;

export const MainInfoItem = styled.div`
    display: flex;
    flex-direction: column;
`;

export const InfoItem = styled.div`
    display: flex;
    align-items: center;
    margin: 0.5rem 0;
    width: 100%;

    svg {
        margin-right: 0.5rem;
    }

    span {
        font-size: 1.2rem;
        color: #333;
    }
`;
