import {
    FaArrowsAltV,
    FaArrowsAltH,
    FaWindowMaximize,
    FaDoorOpen,
} from "react-icons/fa";
import { Container, InfoItem, MainInfoItem } from "./Styled";
import { useCallback, useMemo, useState } from "react";
import { calculatePaint } from "../../utils/utils";
import toast from "react-hot-toast";
import ButtonCustom from "../Button/Button";
import Modal from "../Modal/Modal";

const WallCard = ({ height, width, windows, doors, id, area, walls }) => {
    const [totalPaint, setTotalPaint] = useState(null);
    const [paintCans, setPaintCans] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [error, setError] = useState(false);

    const calculate = useCallback(() => {
        try {
            const { totalLiters, cans } = calculatePaint([
                { height, width, windows, doors, area },
            ]);
            console.log("totalLiters, cans", totalLiters, cans);
            setTotalPaint(totalLiters);
            setPaintCans(cans);
        } catch (err) {
            if (err.message) {
                setError(true);
            }
            console.log(err.message);
            toast.error(err.message);
        }
    }, [area, doors, height, width, windows]);

    const paintCansList = useMemo(() => {
        return paintCans.map((can, index) => <li key={index}>{can} L</li>);
    }, [paintCans]);

    const handleOpenModal = () => setShowModal(true);
    const handleCloseModal = () => setShowModal(false);

    const handleButtonClick = () => {
        calculate();
        handleOpenModal();
    };
    return (
        <Container>
            <MainInfoItem>
                <InfoItem>
                    <FaArrowsAltV size={24} />
                    <span>Altura: {height}m</span>
                </InfoItem>
                <InfoItem>
                    <FaArrowsAltH size={24} />
                    <span>Largura: {width}m</span>
                </InfoItem>
                <InfoItem>
                    <FaWindowMaximize size={24} />
                    <span>Janelas: {windows}</span>
                </InfoItem>
                <InfoItem>
                    <FaDoorOpen size={24} />
                    <span>Portas: {doors}</span>
                </InfoItem>
            </MainInfoItem>
            <ButtonCustom
                onClick={handleButtonClick}
                label={"Calcular Tinta"}
            ></ButtonCustom>
            {!error && (
                <Modal
                    show={showModal}
                    handleClose={handleCloseModal}
                    totalPaint={totalPaint !== null && totalPaint}
                    paintCans={paintCansList}
                />
            )}
        </Container>
    );
};

export default WallCard;
