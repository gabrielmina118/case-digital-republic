import { create } from "zustand";

const useAddWallModal = create((set) => ({
    isOpen: false,
    onOpen: () => set({ isOpen: true }),
    onClose:() => set({isOpen:false})
}));

export default useAddWallModal
