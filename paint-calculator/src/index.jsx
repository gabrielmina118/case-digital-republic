import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import ToasterProvider from './providers/ToasterProvider';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
        <ToasterProvider />
        <App />
    </React.StrictMode>
);
