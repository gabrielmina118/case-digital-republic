export const calculatePaint = (walls) => {
  console.log('Walls',walls)
  const totalArea = walls.reduce((sum, wall) => sum + wall.area, 0);
  const totalWindowArea = walls.reduce((sum, wall) => sum + wall.windows * 2.4, 0);
  const totalDoorArea = walls.reduce((sum, wall) => sum + wall.doors * 1.52, 0);

  if (totalDoorArea + totalWindowArea > 0.5 * totalArea) {
    throw new Error(
      'A área total das portas e janelas não podem ser superiores a 50% da área da parede.',
    );
  }

  // Calculo da área pintada
  const paintTableArea = totalArea - totalWindowArea - totalDoorArea;

  // Calculo da quantidade total de tinta necessaria
  const totalLiters = paintTableArea / 5;

  const canSizes = [18, 3.6, 2.5, 0.5];

  // Calculando a combinacao ideal de latas
  const cans = [];
  let remainingPaint = totalLiters;

  for (const size of canSizes) {
    while (remainingPaint >= size) {
      cans.push(size);
      remainingPaint -= size;
    }
  }

  // Caso apos o loop ainda houver tinta restante , adicionar 0.5 para cobrir
  if (remainingPaint > 0) {
    cans.push(0.5);
  }

  return { totalLiters, cans };
};

export const validateWall = ({ height, width, doors }) =>{
  const area = height * width;

  if (area < 1 || area > 50) {
    throw new Error('A área da parede deve estar entre 1 e 50 metros quadrados');
  }

  if (doors > 0 && height <= 2.2) {
    throw new Error('A altura da parede com porta deve ser no mínimo 2,2 metros.');
  }
  return area
};
